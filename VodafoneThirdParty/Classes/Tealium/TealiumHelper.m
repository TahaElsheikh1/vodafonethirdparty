//
//  TealiumHelper.m
//  Ana Vodafone
//
//  Created by Taha on 1/17/18.
//  Copyright © 2018 VIS. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "TealSoasta.h"
@import TealiumIOS;

#import "TealiumHelper.h"

NSString *const TEALIUM_INSTANCE_ID = @"vodafone";

@implementation TealiumHelper
    
    static TealiumHelper * _sharedInstance;
    
+ (instancetype) sharedInstance {
    
    if (!_sharedInstance){
        _sharedInstance = [[TealiumHelper alloc] init];
    }
    
    return _sharedInstance;
}
    
+ (void) startTracking {
    // TODO: Update the "profile" reference with the correct profile name for your market
    TEALConfiguration *configuration = [TEALConfiguration configurationWithAccount:@"vodafone"
                                                                           profile:@"eg-myvfapp-ios"
                                                                       environment:@"prod"];
    
    
    [configuration setRemoteCommandsEnabled:true];
    
    
    Tealium *tealiumInstance = [Tealium newInstanceForKey:TEALIUM_INSTANCE_ID configuration:configuration];
    //    [tealiumInstance setLifecycleAutotrackingIsEnabled:YES];
    [tealiumInstance setDelegate:[TealiumHelper sharedInstance]];
    // Init SOASTA remote command module
    //    TEALSoastaRemoteCommand *tealiumSoasta = [TEALSoastaRemoteCommand sharedInstance];
    //    [tealiumSoasta enable: TEALIUM_INSTANCE_ID];
}
    
+ (void) trackEventWithTitle:(NSString *)title dataSources:(NSDictionary *)data {
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithDictionary:data];
    
    //    NSString *ratePlan =([AppUser sharedInstance].userModel.tariffModelName)?[AppUser sharedInstance].userModel.tariffModelName:@"";
    
    //    NSString *trimmed = [ratePlan stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    //    NSString *userRatePlan = [[AppUser sharedInstance] getUserTier]  ;
    
    [dic addEntriesFromDictionary:@{@"Rate_Plan":[TealiumHelper sharedInstance].userRatePlan}];
    NSLog(@"#$Tealium trackEventWithTitle dic : %@",dic);
    
    [[Tealium instanceForKey:TEALIUM_INSTANCE_ID] trackEventWithTitle:title dataSources:dic];
}
    
+ (void) trackViewWithTitle:(NSString *)title dataSources:(NSDictionary *)data {
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithDictionary:data];
    
    //    NSString *ratePlan =([AppUser sharedInstance].userModel.tariffModelName)?[AppUser sharedInstance].userModel.tariffModelName:@"";
    
    NSString *trimmed = [[TealiumHelper sharedInstance].ratePlan stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    [dic addEntriesFromDictionary:@{@"Rate_Plan":trimmed}];
    
    NSLog(@"#$Tealium trackViewWithTitle dic : %@",dic);
    [[Tealium instanceForKey:TEALIUM_INSTANCE_ID] trackViewWithTitle:title dataSources:dic];
}
    
+(void) addPersistentDataSources:(NSDictionary *)data{
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithDictionary:data];
    
    [[Tealium instanceForKey:TEALIUM_INSTANCE_ID] addPersistentDataSources:dic];
}
    
+ (void) stopTracking{
    
    [Tealium destroyInstanceForKey:TEALIUM_INSTANCE_ID];
    
}
    
#pragma mark - OPTIONAL TEALIUM DELEGATE
    
- (BOOL) tealium:(Tealium *)tealium shouldDropDispatch:(TEALDispatch *)dispatch {
    
    // Add optional tracking suppression logic here - returning YES will destroy
    // any processed dispatch so some conditional must eventually return NO
    
    return NO;
    
}
    
- (BOOL) tealium:(Tealium *)tealium shouldQueueDispatch:(TEALDispatch *)dispatch {
    
    // Add optional queuing / saving logic here - returning YES will save
    // a dispatch so some condition must eventually return NO.
    
    return NO;
    
}
    
- (void) tealium:(Tealium *)tealium didQueueDispatch:(TEALDispatch *)dispatch{
    
    // Add optional code here to respond to queuing of dispatches.
    
}
    
- (void) tealium:(Tealium *)tealium didSendDispatch:(TEALDispatch *)dispatch{
    
    // Add optional code here to respond to sent dispatches.
    
}
    
- (void) tealium:(Tealium *)tealium webViewIsReady:(id)webView{
    
    // Use this to interact with the Tag Management Dispatcher's webview - available only if Tag Management enabled via remote settings.
    //[TealiumHelper trackViewWithTitle: @"Homescreen" dataSources:@{@"screen_title" : @"Homescreen"}];
}
    
#pragma mark - EXAMPLE METHODS USING OTHER TEALIUM APIS
    
+ (void) incrementLifetimeValueForKey:(NSString *)key amount:(int)number{
    
    NSDictionary *persistentData = [[Tealium instanceForKey:TEALIUM_INSTANCE_ID] persistentDataSourcesCopy];
    
    int oldNumber = [persistentData[key] intValue];
    
    int newNumber = oldNumber + number;
    
    [[Tealium instanceForKey:TEALIUM_INSTANCE_ID] addPersistentDataSources:@{key:@(newNumber)}];
    
    NSLog(@"%s Current lifetime value for %@ is: %i", __FUNCTION__, key, newNumber);
    
}
    
    @end

