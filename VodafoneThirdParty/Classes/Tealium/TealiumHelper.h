//
//  TealiumHelper.h
//  Ana Vodafone
//
//  Created by Taha on 1/17/18.
//  Copyright © 2018 VIS. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <MPulse/MPulse.h>
#import <TealiumIOS/TealiumIOS.h>

//@import TealiumIOS;


@interface TealiumHelper : NSObject<TealiumDelegate>
    
    @property (nonatomic,strong) NSString* ratePlan;
    @property (nonatomic,strong) NSString* userRatePlan;
    
+ (instancetype) sharedInstance;
    
+ (void) startTracking;
    
+ (void) trackEventWithTitle:(NSString *)title dataSources:(NSDictionary *)data;
    
+ (void) trackViewWithTitle:(NSString *)title dataSources:(NSDictionary *)data;
    
+ (void) stopTracking;
    
+ (void) incrementLifetimeValueForKey:(NSString *)key amount:(int)number;
    
+ (void) addPersistentDataSources:(NSDictionary *)data;
    
    @end

