//
//  GMSurveyButton.m
//  ChatHeads
//
//  Created by Ahmed Omran on 12/22/14.
//  Copyright (c) 2014 Matthias Hochgatterer. All rights reserved.
//

#import "SelectionButton.h"
#import "CustomUtility.h"

#define DEF_HORIZONTAL_SPACING		5
#define DEF_VERTICAL_SPACING		5

#define DEF_CHECKBOX_IMG_WIDTH		10
#define DEF_CHECKBOX_IMG_HEIGHT		10

@interface SelectionButton()
{
	
}

@property (nonatomic, strong) UIImageView	*checkImgView;
@property (nonatomic, strong) UIImageView	*fGCheckImgView;
@property (nonatomic, strong) UILabel		*selectionLabel;

@end

@implementation SelectionButton

#pragma mark - initialization

-(id) init//WithFrame:(CGRect)frame
{
	self = [super init];
	if (self){
		self.horizontalSpacing = -1;
		self.textVerticalSpacing = -1;
	}
	return self;
}

-(id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:(CGRect)frame];
	if (self){
		self.horizontalSpacing = -1;
		self.textVerticalSpacing = -1;
		
//		[self setupCheckBox];
	}
	return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
	
//	[super drawRect:rect];
	
	// 1- setup checkbox, the default drawing
	[self setupSelectionButton];
	
	// 2- custom drwaing by the lib users
	if ([self.dataSource respondsToSelector:@selector(modifySelectionBtn:withLabel:andImgView:)]) {
		[self.dataSource modifySelectionBtn:self withLabel:self.selectionLabel andImgView:self.checkImgView];
	}
	
	// set the initial state
	[self updateSelectionButtonUIState];
}

-(void) refreshCheckbox
{
	//	self.contentMode = UIViewContentModeRedraw;
	[self setNeedsDisplay];
}


#pragma mark - setup methods

-(void) setupSelectionButton
{
    // 0- calculating sizes
	if (self.horizontalSpacing == -1) {
		self.horizontalSpacing = DEF_HORIZONTAL_SPACING;
	}
	
	if (self.textVerticalSpacing == -1) {
		self.textVerticalSpacing = DEF_VERTICAL_SPACING;
	}
	
	CGSize checkImgSize;
	
	if (self.selectedImgSize.width > 0 && self.selectedImgSize.height > 0) {
		checkImgSize = CGSizeMake(MIN(self.selectedImgSize.width, self.frame.size.width), MIN(self.selectedImgSize.height, self.frame.size.height));
	}
	else // use the default sizes
	{
		checkImgSize.height = DEF_CHECKBOX_IMG_HEIGHT;
		checkImgSize.width = DEF_CHECKBOX_IMG_WIDTH;
	}
    
    if(_selectionButtonLangOrientation == SelectionButton_RTL)
    {
        self.checkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - (self.horizontalSpacing + checkImgSize.width), (self.frame.size.height - checkImgSize.height) / 2, checkImgSize.width, checkImgSize.height)];
		
        self.selectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.horizontalSpacing, self.textVerticalSpacing, self.frame.size.width - (checkImgSize.width + (3 * self.horizontalSpacing)), self.frame.size.height - (2*self.textVerticalSpacing))];
        
        self.selectionLabel.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        // 1- adding check img
        self.checkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.horizontalSpacing, (self.frame.size.height - checkImgSize.height) / 2, checkImgSize.width, checkImgSize.height)];
        
        // 3- adding label
        self.selectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(2*(self.horizontalSpacing) + checkImgSize.width, self.textVerticalSpacing, self.frame.size.width - (checkImgSize.width + (3 * self.horizontalSpacing)), self.frame.size.height - (2*self.textVerticalSpacing))];
		
        self.selectionLabel.textAlignment = NSTextAlignmentLeft;
    }
    
	// 1- adding check img
    self.checkImgView.backgroundColor = [UIColor clearColor];

	self.checkImgView.image = self.unSelectedImg;
	[self.checkImgView setHighlightedImage:self.selectedImg];
	
	[self addSubview:self.checkImgView];
	
	// 2- adding label
	self.selectionLabel.backgroundColor = [UIColor clearColor];

	self.selectionLabel.text = self.itemDataObject.name;
	if (self.itemDataObject.attributedName) {
		self.selectionLabel.attributedText = self.itemDataObject.attributedName;
	}
	
	self.selectionLabel.textColor = [UIColor blackColor];
	self.selectionLabel.font = self.font;
	
	[self addSubview:self.selectionLabel];
	
	// 4- add the target action
	[self addTarget:self action:@selector(checkBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - btn pressed
-(IBAction)checkBtnPressed:(id)sender
{
	if ((self.type == RadioButton) && (self.itemDataObject.selected == YES)) {
		return;
	}

	self.itemDataObject.selected = !self.itemDataObject.selected;
	
	[self updateSelectionButtonUIState];
	
	if ([self.delegate respondsToSelector:@selector(checkedItem:)]){
		[self.delegate checkedItem:self];
	}
	else{ // default behavior
		[self dafaultCheckbtnBehavior];
	}
}

-(void) dafaultCheckbtnBehavior
{
//	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Default Behavior" message:@"Default Message" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//	[alert show];
    DLog(@"Default behaviour");
}

#pragma mark - handle actions

-(void) updateSelectionButtonUIState
{
	if ([self.dataSource respondsToSelector:@selector(selectionButtonStateChanged:)]) {
		[self.dataSource selectionButtonStateChanged:self];
		return;
	}
	
	if (self.itemDataObject.selected)
	{
		if (self.selectWithAnimation)
		{
			if (self.type == CheckBox) {
				[self checkboxSelectionAnimation];
			}
			else
			{
				[self radioButtonSelectionAnimation];
			}
		}
		else
		{
			[self.checkImgView setHighlighted:self.itemDataObject.selected];
		}
	}
	else
	{
		if (self.unSelectWithAnimation)
		{
			if (self.type == CheckBox) {
				[self checkboxUnSelectionAnimation];
			}
			else
			{
				[self radioButtonUnSelectionAnimation];
			}
		}
		else
		{
			[self.checkImgView setHighlighted:self.itemDataObject.selected];
		}
	}
}

#pragma mark - selection animations

-(void) addCheckboxFGImg
{
	[self.fGCheckImgView removeFromSuperview];
	self.fGCheckImgView = nil;
	
	// calculate fgImgRect
	CGRect fgCheckImgRect;
	if (self.type == RadioButton)
	{
		fgCheckImgRect = CGRectMake(self.checkImgView.frame.origin.x, self.checkImgView.frame.origin.y, self.checkImgView.frame.size.width , self.checkImgView.frame.size.height);
	}
	else // decrease the FGImgview size abit in case of checkbox type
	{
		fgCheckImgRect = CGRectMake(self.checkImgView.frame.origin.x+2, self.checkImgView.frame.origin.y+2, self.checkImgView.frame.size.width -4, self.checkImgView.frame.size.height-4);
	}
	
	// adding fgImgview
	self.fGCheckImgView = [[UIImageView alloc] initWithFrame:fgCheckImgRect];
	self.fGCheckImgView.layer.cornerRadius = self.fGCheckImgView.frame.size.width / 2.0;
	self.fGCheckImgView.backgroundColor = self.selectedImgBgColor;
	[self addSubview:self.fGCheckImgView];
}

-(void) checkboxSelectionAnimation
{
	// add the FGImg for animation
	[self addCheckboxFGImg];
	
	[self.checkImgView setHighlighted:self.itemDataObject.selected];
	
	CGRect finalFGImgRect = self.fGCheckImgView.frame;
	finalFGImgRect.size.width = 0.0;
	finalFGImgRect.origin.x += (self.fGCheckImgView.frame.size.width);
	
	[UIView animateWithDuration:0.7 animations:^{
		
		self.fGCheckImgView.frame = finalFGImgRect;
		
	} completion:^(BOOL finished) {
		
		[self.fGCheckImgView removeFromSuperview];
		self.fGCheckImgView = nil;
		
	}];
}

-(void) checkboxUnSelectionAnimation
{
	// add the FGImg for animation
	[self addCheckboxFGImg];
	
	CGRect finalFGImgRect = self.fGCheckImgView.frame;
	CGRect initialFGImgRect = self.fGCheckImgView.frame;
	initialFGImgRect.size.width = 0.0;
	initialFGImgRect.origin.x += (self.fGCheckImgView.frame.size.width);
	
	self.fGCheckImgView.frame = initialFGImgRect;
	
	[UIView animateWithDuration:0.7 animations:^{
		
		self.fGCheckImgView.frame = finalFGImgRect;
		
	} completion:^(BOOL finished) {
		
		[self.checkImgView setHighlighted:self.itemDataObject.selected];
		
		[self.fGCheckImgView removeFromSuperview];
		self.fGCheckImgView = nil;
		
	}];
}

-(void) radioButtonSelectionAnimation
{
	[self addCheckboxFGImg];
	
	self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	
	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
	{
		[UIView animateWithDuration:0.7 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
			self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.45, 0.45);
		} completion:^(BOOL finished) {
			
			self.fGCheckImgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
			[self.fGCheckImgView removeFromSuperview];
			self.fGCheckImgView = nil;
			[self.checkImgView setHighlighted:self.itemDataObject.selected];
			
		}];
	}
	else
	{
		[UIView animateWithDuration:0.3 animations:^{
			self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.45, 0.45);
		} completion:^(BOOL finished) {
			self.fGCheckImgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
			[self.fGCheckImgView removeFromSuperview];
			self.fGCheckImgView = nil;
			[self.checkImgView setHighlighted:self.itemDataObject.selected];
		}];
	}
	
	
	
	//	[UIView animateWithDuration:0.4 animations:^{
	//
	//		self.fGCheckImgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
	//
	//	} completion:^(BOOL finished) {
	//
	//		[UIView animateWithDuration:0.2 animations:^{
	//
	//			self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.5, 0.5);
	//
	//		} completion:^(BOOL finished) {
	//
	//			self.fGCheckImgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
	//			[self.fGCheckImgView setHidden:YES];
	//			[self.checkImgView setHighlighted:YES];
	//		}];
	//	}];
	
	
}

-(void) radioButtonUnSelectionAnimation
{
	[self addCheckboxFGImg];
	
	[self.checkImgView setHighlighted:self.itemDataObject.selected];
	self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.5, 0.5);
	
	[UIView animateWithDuration:0.3 animations:^{
		self.fGCheckImgView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	} completion:^(BOOL finished) {
		
		[self.fGCheckImgView removeFromSuperview];
		self.fGCheckImgView = nil;
	}];
}

#pragma mark - setters

-(void) setSelectedImg:(UIImage *)selectedImg
{
	[self.checkImgView setHighlightedImage:selectedImg];
	_selectedImg = selectedImg;
}

-(void) setUnSelectedImg:(UIImage *)unSelectedImg
{
	self.checkImgView.image = unSelectedImg;
	_unSelectedImg = unSelectedImg;
}

-(void) setItemDataObject:(SelectionButtonData *)itemDataObject
{
	_itemDataObject = itemDataObject;
	_uncheckBtn = _itemDataObject.selected;
}

-(void) setUncheckBtn:(BOOL)uncheckBtn
{
	_uncheckBtn = uncheckBtn;
	
	if (_itemDataObject) {
		_itemDataObject.selected = !uncheckBtn;
	}
	
	[self updateSelectionButtonUIState];
}

@end
