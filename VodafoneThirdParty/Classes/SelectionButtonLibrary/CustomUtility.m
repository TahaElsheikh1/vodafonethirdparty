//
//  CustomUtility.m
//  ChatHeads
//
//  Created by Ahmed Omran on 1/15/15.
//  Copyright (c) 2015 Matthias Hochgatterer. All rights reserved.
//

#import "CustomUtility.h"
#include <CommonCrypto/CommonCrypto.h>
#import "NSData+CommonCrypto.h"

@implementation CustomUtility
/*
 +(void) addBlurEffectToView:(UIView*)view withImageName:(UIImage*)image
 {
	// show image
	view.backgroundColor = [UIColor colorWithPatternImage:image];
	
	// create effect
	UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
	
	// add effect to an effect view
	UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
	effectView.frame = view.frame;
	
	// add the effect view to the image view
	[view addSubview:effectView];
 }
 
 +(void) addVibrancyEffectToView:(UIView*)view withImageName:(UIImage*)image
 {
	// show image
	view.backgroundColor = [UIColor colorWithPatternImage:image];
	
	// create blur effect
	UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
	
	// create vibrancy effect
	UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
	
	// add blur to an effect view
	UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
	effectView.frame = view.frame;
	
	// add vibrancy to yet another effect view
	UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
	effectView.frame = view.frame;
	
	// add both effect views to the image view
	[view addSubview:effectView];
	[view addSubview:vibrantView];
 }
 */
+(void) addErrorBorderForView:(UIView*)view
{
    UIColor *borderColor = [UIColor redColor];
    view.layer.borderColor = borderColor.CGColor;
    view.layer.borderWidth = 0.6;
    view.layer.cornerRadius = 7;
}

+(void) removeErrorBorderForView:(UIView*)view
{
    UIColor *borderColor = [CustomUtility colorFromHexString:@"aaaaaa"];
    view.layer.borderColor = borderColor.CGColor;
    view.layer.borderWidth = 0.6;
    view.layer.cornerRadius = 7;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    
}

+ (UIColor *)colorOppositeFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    float red = (255.0-((rgbValue & 0xFF0000) >> 16))/255.0;
    float green = (255.0-(((rgbValue & 0xFF00) >> 8)))/255.0;
    float blue = (255.0-(rgbValue & 0xFF))/255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

+ (UIImage*)imageWithColor:(UIColor*)color{
    
    CGSize imageSize = CGSizeMake(64, 64);
    UIColor *fillColor = color;
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [fillColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSString *)urlencode: (NSString *) url {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[url UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

+(NSString *)tripleDesEncryptString:(NSString *)input
                                key:(NSString *)key
                              error:(NSError **)error{
    NSParameterAssert(input);
    NSParameterAssert(key);
    
    NSData *inputData = [input dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    size_t outLength;
    
    NSAssert(keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(inputData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus
    result = CCCrypt(kCCEncrypt, // operation
                     kCCAlgorithm3DES, // Algorithm
                     kCCOptionPKCS7Padding | kCCOptionECBMode, // options
                     keyData.bytes, // key
                     keyData.length, // keylength
                     nil,// iv
                     inputData.bytes, // dataIn
                     inputData.length, // dataInLength,
                     outputData.mutableBytes, // dataOut
                     outputData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:@"com.your_domain.your_project_name.your_class_name."
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    
    return [self base64forData:outputData];
}

+(NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {  value |= (0xFF & input[j]);  }  }  NSInteger theIndex = (i / 3) * 4;  output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
}

+(NSString *)getUserPassword:(NSString *)pass {
    
    NSData *data = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    data = [data SHA512Hash];
    
    data = [data base64EncodedDataWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    
   return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}
@end
