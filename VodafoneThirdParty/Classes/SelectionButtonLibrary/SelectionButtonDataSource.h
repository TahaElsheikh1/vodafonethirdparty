//
//  Header.h
//  SelectionButtonLibrary
//
//  Created by Ahmed Omran on 6/17/15.
//  Copyright (c) 2015 VIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectionButton;

@protocol SelectionButtonDataSource <NSObject>

// will call this method when btn pressed

@optional

-(void) modifySelectionBtn:(SelectionButton*)selectedButton withLabel:(UILabel*)label andImgView:(UIImageView*)imgView;
-(void) selectionButtonStateChanged:(SelectionButton*)selectedButton;

@end