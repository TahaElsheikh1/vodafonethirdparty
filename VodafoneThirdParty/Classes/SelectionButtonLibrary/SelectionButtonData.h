//
//  CheckBoxObject.h
//  Toamor
//
//  Created by Ahmed Omran on 5/13/14.
//  Copyright (c) 2014 FL. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  The data object that is used in the selection button class
 */
@interface SelectionButtonData : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSAttributedString* attributedName;
@property (nonatomic) BOOL selected;
@property (nonatomic) int itemId;

@end
