//
//  Header.h
//  SelectionButtonLibrary
//
//  Created by Ahmed Omran on 6/17/15.
//  Copyright (c) 2015 VIS. All rights reserved.
//

@class SelectionButton;

@protocol SelectionButtonDelegate <NSObject>

// will call this method when btn pressed

@optional
-(void) checkedItem:(SelectionButton*)selectedButton;

@end