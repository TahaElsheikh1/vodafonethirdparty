//
//  GMSurveyButton.h
//  ChatHeads
//
//  Created by Ahmed Omran on 12/22/14.
//  Copyright (c) 2014 Matthias Hochgatterer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SelectionButton.h"
#import "SelectionButtonData.h"

#import "SelectionButtonDelegate.h"
#import "SelectionButtonDataSource.h"

#define GREEN_COLOR		[UIColor colorWithRed:(46.0/255.0) green:(111.0/255.0) blue:(1.0/255.0) alpha:1]

/*!
	@typedef SelectionButtonType
	@brief enumeration for the selection button type
	@discussion its value determines if the selection button is checkbox or Radio button
 */
typedef enum
{
	/*!
	 *  type is checkbox
	 */
	CheckBox = 1,
	/*!
	 *  type is radio button
	 */
	RadioButton
}SelectionButtonType;

/*!
	@typedef LangOrientationSelectionButton
	@brief Enumeration for the language selection (RTL or LTR)
	@discussion its value determine wheather button will be in RTL or LTR mode
 */
typedef enum
{
	/*!
	 *  LTR mode
	 */
    SelectionButton_LTR = 1,
	
	/*!
	 *  RTL mode
	 */
    SelectionButton_RTL
}LangOrientationSelectionButton;


/*!
 *  SelectionButton is a custom UI component that provids the developer with a selection button ui element that could be used as a checkbox or radio button
 */

@interface SelectionButton : UIButton

@property (nonatomic, assign) id<SelectionButtonDelegate> delegate;
@property (nonatomic, assign) id<SelectionButtonDataSource> dataSource;

@property (nonatomic, strong) SelectionButtonData *itemDataObject;

@property (nonatomic) SelectionButtonType type;

@property (nonatomic,assign) LangOrientationSelectionButton selectionButtonLangOrientation;

/*!
 *  Set to Yes to animate viewing the selection image
 */
@property (nonatomic) BOOL selectWithAnimation;

/*!
 *  Set to Yes to animate viewing the unSelection image
 */
@property (nonatomic) BOOL unSelectWithAnimation;

@property (nonatomic, strong) UIImage *selectedImg;
@property (nonatomic, strong) UIImage *unSelectedImg;
@property (nonatomic, strong) UIColor *selectedImgBgColor;

@property (nonatomic) float horizontalSpacing;
@property (nonatomic) float textVerticalSpacing;
@property (nonatomic) CGSize selectedImgSize;

@property (nonatomic, strong) UIFont *font;

@property (nonatomic) BOOL uncheckBtn;

// methods to be overriedden to change the implementation

/*!
 *  Sets up the selection button inernal structure and could be re-implemented in a sub-class to change the internal look & feel
 */
-(void) setupSelectionButton;

/*!
 *  Refreshs the selection button UI upon changing any UI elemnt
 */
-(void) refreshCheckbox;

@end