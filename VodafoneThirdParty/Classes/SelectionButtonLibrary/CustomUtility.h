//
//  CustomUtility.h
//  ChatHeads
//
//  Created by Ahmed Omran on 1/15/15.
//  Copyright (c) 2015 Matthias Hochgatterer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_OLDER_DEVICE ([UIScreen mainScreen].bounds.size.height < 568) ? YES : NO


// debug log and always log, AFNetworking definitions, flag to indicate the app state (debug, release)
#ifdef DEBUG
#   define DLog(...) NSLog(__VA_ARGS__)
#	define ADD_BUILD_STATE		@"debug"
#	define	_AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES_
#else
#   define DLog(...) /* */
#	define ADD_BUILD_STATE		@"release"
#	define _AFNETWORKING_PIN_SSL_CERTIFICATES_ 1
#endif
#define ALog(...) NSLog(__VA_ARGS__)


/*!
 * CustomUtility class is helper/utility class that contains some Macros to check the running iOS version number and to also some methods to make conversions between color and hex code
 */

@interface CustomUtility : NSObject

/*
+(void) addBlurEffectToView:(UIView*)view withImageName:(UIImage*)image;

+(void) addVibrancyEffectToView:(UIView*)view withImageName:(UIImage*)image;
*/

/*!
*  Adds a red border to a view
*
*  @param view the view that we want to add a red border to it
*/
+(void) addErrorBorderForView:(UIView*)view;

/*!
 *  Removes the border from a view
 *
 *  @param view the view that we want to remove its border
 */
+(void) removeErrorBorderForView:(UIView*)view;

/*!
 *  Gets the UIColor from a hex string
 *
 *  @param hexString hex string for a color
 *
 *  @return UIColor for the hex string
 */
+ (UIColor *)colorFromHexString:(NSString *)hexString;

/*!
 *  Gets the color opposite for a hex string color
 *
 *  @param hexString coloe hex string
 *
 *  @return the opposit color
 */
+ (UIColor *)colorOppositeFromHexString:(NSString *)hexString;

+ (UIImage*)imageWithColor:(UIColor*)color;

+ (NSString *)urlencode: (NSString *) url;

+(NSString *)tripleDesEncryptString:(NSString *)input
                                key:(NSString *)key
                              error:(NSError **)error;

+(NSString *)getUserPassword:(NSString *)pass ; 

@end
