//
//  SliderView.h
//  Slider
//
//  Created by Mathieu Bolard on 02/02/12.
//  Copyright (c) 2012 Streettours. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBSliderLabel;
@protocol MBSliderViewDelegate;

@interface MBSliderView : UIView {
    UISlider *_slider;
    id<MBSliderViewDelegate> _delegate;
    BOOL _sliding;
}

@property (nonatomic) IBOutlet id<MBSliderViewDelegate> delegate;
@property (nonatomic) BOOL enabled;

@end

@protocol MBSliderViewDelegate <NSObject>

- (void) sliderDidSlideRight:(MBSliderView *)slideView;
- (void) sliderDidSlideLeft:(MBSliderView *)slideView;
- (void) sliderChangedDelegate:(UISlider *)sender;



@end




@interface MBSliderLabel : UILabel {
    NSTimer *animationTimer;
    CGFloat gradientLocations[3];
    int animationTimerCount;
    BOOL _animated;
}

@property (nonatomic, assign, getter = isAnimated) BOOL animated;

@end