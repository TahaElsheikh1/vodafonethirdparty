# VodafoneThirdParty

[![CI Status](https://img.shields.io/travis/VodafoneEgypt/VodafoneThirdParty.svg?style=flat)](https://travis-ci.org/VodafoneEgypt/VodafoneThirdParty)
[![Version](https://img.shields.io/cocoapods/v/VodafoneThirdParty.svg?style=flat)](https://cocoapods.org/pods/VodafoneThirdParty)
[![License](https://img.shields.io/cocoapods/l/VodafoneThirdParty.svg?style=flat)](https://cocoapods.org/pods/VodafoneThirdParty)
[![Platform](https://img.shields.io/cocoapods/p/VodafoneThirdParty.svg?style=flat)](https://cocoapods.org/pods/VodafoneThirdParty)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

VodafoneThirdParty is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'VodafoneThirdParty'
```

## Author

VodafoneEgypt, taha.abdelraouf-kotb@vodafone.com

## License

VodafoneThirdParty is available under the MIT license. See the LICENSE file for more info.
