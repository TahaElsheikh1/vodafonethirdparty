//
//  VFAppDelegate.h
//  VodafoneThirdParty
//
//  Created by VodafoneEgypt on 10/29/2018.
//  Copyright (c) 2018 VodafoneEgypt. All rights reserved.
//

@import UIKit;

@interface VFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
