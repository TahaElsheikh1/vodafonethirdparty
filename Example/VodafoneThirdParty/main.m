//
//  main.m
//  VodafoneThirdParty
//
//  Created by VodafoneEgypt on 10/29/2018.
//  Copyright (c) 2018 VodafoneEgypt. All rights reserved.
//

@import UIKit;
#import "VFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VFAppDelegate class]));
    }
}
